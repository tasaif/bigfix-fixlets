﻿function GetOrCreate-Item {
    Param(
        [string]$Path
    )
    $exists = Test-Path -Path $Path
    if ($exists){
        return get-item -Path $path
    }
    $retval = new-item -Path $path
    return $retval
}

function psobject_to_hash($object){
    $retval = @{}
    $keys = $object.psobject.properties | % { $_.Name }
    ForEach($key in $keys){
        $retval[$key] = iex("`$object.$key")
    }
    return $retval
}

function Get-UpdateLog {
    $Session = New-Object -ComObject "Microsoft.Update.Session" 
    $Searcher = $Session.CreateUpdateSearcher() 
    $historyCount = $Searcher.GetTotalHistoryCount() 
    $update_logs = $Searcher.QueryHistory(0, $historyCount)
    $retval = @{}
    ForEach($log in $update_logs){
        $existing_record = $retval[$log.Title]
        if ($existing_record -eq $null){
            $retval[$log.Title] = $log
        } else {
            if ($existing_record.Date -lt $log.Date){
                $retval[$log.Title] = $log;
            }
        }
    }
    return $retval
}

function Write-Registry {
    Param(
        [String]$path,
        [Hashtable]$hash
    )
    New-Item -Path $path -Force | Out-Null
    ForEach ($title in $hash.Keys){
        New-Item -Path "$path/$title" | Out-Null
        $subkeys = psobject_to_hash($hash[$title])
        ForEach($subkey in $subkeys.Keys){
            New-ItemProperty -Path "$path/$title" -Name $subkey -Value $subkeys[$subkey] | Out-Null
        }
    }
}

function main2 {
    $update_logs = Get-UpdateLog
    Write-Registry -path "HKLM:\SOFTWARE\LBNL\ICUSW\Windows\UpdateHistory" -hash $update_logs
}

main2