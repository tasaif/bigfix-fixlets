Windows (in elevated command prompt)
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -ExecutionPolicy Bypass -Command "$cmd = \"`$baselines='icusw';\"; $cmd += ((New-Object System.Net.WebClient).DownloadString('https://bitbucket.org/tasaif/bigfix-fixlets/raw/master/subscribe_to_baselines/subscribe.ps1')); iex $cmd"

Mac (in Terminal)
curl -sSL https://bitbucket.org/tasaif/bigfix-fixlets/raw/master/subscribe_to_baselines/subscribe.sh | sudo bash -s icusw

