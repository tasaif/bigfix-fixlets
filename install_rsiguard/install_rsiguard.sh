#!/bin/bash
url="http://setup.lbl.gov/files/support/RSIGuard-LBNL-v5.1.4aCM.dmg"
dmg="rsiguard.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
# Remove old
killall RSIGuard
rm -rf /Applications/RSIGuard.app
# Install new
cp -r ./$dir/RSIGuard.app /Applications/RSIGuard.app
# Cleanup
hdiutil detach $dir
rm $dmg
rm -rf $dir
