﻿Param(
    [string]$email,
    [string]$files,
    [string]$subject,
    [string]$beslog,
    [string]$processes,
    [string]$services,
    [string]$updates
)

function repl {
    while ($true){
        $input = Read-Host -Prompt "repl"
        if ($input -eq "exit"){
            return 0
        }
        Invoke-Expression $input
    }
}

function Process-Filelist {
    Param(
        [string]$Filelist
    )
    $retval = $Filelist.split([environment]::NewLine) | where { $_ -ne "" } | where { Test-Path $_ } | Get-Item
    return $retval
}

function email {
    Param(
        [string]$fromaddress,
        [string]$toaddress,
        [string]$ccaddress,
        [string]$bccaddress,
        [string]$subject,
        [System.Collections.ArrayList]$file_paths,
        [string]$body
    )
    if ($beslog -eq "Yes"){
        $log_dir = "C:\Program Files (x86)\BigFix Enterprise\BES Client\__BESData\__Global\Logs"
        if (test-path $log_dir){
            # Copy it somewhere else because it's currently in use (the email method must open the file in rw)
            $last_log = (Get-ChildItem $log_dir) | Sort-Object { $_.CreationTime } | select -last 1
            $tmp_location = $env:SystemDrive + "\temp"
            $tmp_log = $tmp_location + "\" + $last_log.Name
            mkdir $tmp_location -ErrorAction SilentlyContinue
            cp $last_log.FullName $tmp_location
            $result = $file_paths += $tmp_log
        }
    }
    $message = new-object System.Net.Mail.MailMessage 
    $message.IsBodyHtml = $True
    $message.From = $fromaddress
    $message.To.Add($toaddress)
    if ($CCaddress -ne ""){
        $message.CC.Add($CCaddress)
    }
    if ($bccaddress -ne ""){
        $message.Bcc.Add($bccaddress)
    }       
    ForEach($file in $file_paths){
        $attach = new-object System.Net.Mail.Attachment($file) 
        $message.Attachments.Add($attach)
    }
    $message.Subject = $Subject
    $message.body = $body
    #$smtp = new-object System.Net.Mail.SmtpClient("smtp.lbl.gov")
    #$result = $smtp.Send($message)
    $emailSmtpServer = "mail.somewhere.com"
    Send-MailMessage -To $toaddress -From $fromaddress -Subject $emailSubject -Body $emailBody -SmtpServer $emailSmtpServer
}


function get_serial_number()
{
    return (Get-WmiObject -Class Win32_BIOS).SerialNumber
}

function get-bigfixlog(){
    $log_dir = "C:\Program Files (x86)\BigFix Enterprise\BES Client\__BESData\__Global\Logs"
    if (-not (test-path $log_dir)){
        return $null
    }
    # Copy it somewhere else because it's currently in use
    $last_log = (Get-ChildItem $log_dir) | Sort-Object { $_.CreationTime } | select -last 1
    $tmp_location = $env:SystemDrive + "\temp"
    $tmp_log = $tmp_location + "\" + $last_log.Name
    mkdir $tmp_location -Force | Out-Null
    Copy-Item $last_log.FullName $tmp_log -Force
    return $tmp_log
}

function get-processesfile(){
    $log_dir = "C:\temp"
    $log_file = "$log_dir\processes.csv"
    mkdir $log_dir -Force | Out-Null
    rm -Path $log_file -Force -ErrorAction SilentlyContinue | Out-Null
    Get-WmiObject Win32_Process | Export-Csv -Path $log_file
    return $log_file
}

function get-servicesfile(){
    $log_dir = "C:\temp"
    $log_file = "$log_dir\services.csv"
    mkdir $log_dir -Force | Out-Null
    rm -Path $log_file -Force -ErrorAction SilentlyContinue | Out-Null
    Get-WmiObject Win32_Service | Export-Csv -Path $log_file
    return $log_file
}

function get-updatesfile(){
    $log_dir = "C:\temp"
    $log_file = "$log_dir\updates.csv"
    mkdir $log_dir -Force | Out-Null
    rm -Path $log_file -Force -ErrorAction SilentlyContinue | Out-Null
    Get-WmiObject -Class "win32_quickfixengineering" | Export-Csv -Path $log_file
    return $log_file
}

# Sample data for testing
#$files = "C:\Users\TASaif\Desktop\main.ps1
#C:\Users\TASaif\Desktop\repl.png
#C:\Users\tasaif\Desktop\foo.txt
#C:\Users\PCUser\Desktop\main.ps1"
#$files = ""
#$email = "tasaif@lbl.gov"
#$beslog = "No"
#$processes = "Yes"
#$services = "Yes"

# Initialization
if ($beslog -eq "Yes"){
    $tmp = [Environment]::NewLine + (get-bigfixlog)
    $files += $tmp
}
if ($processes -eq "Yes"){
    $tmp = [Environment]::NewLine + (get-processesfile)
    $files += $tmp
}
if ($services -eq "Yes"){
    $tmp = [Environment]::NewLine + (get-servicesfile)
    $files += $tmp
}
if ($updates -eq "Yes"){
    $tmp = [Environment]::NewLine + (get-updatesfile)
    $files += $tmp
}
$file_objects = Process-Filelist -Filelist $files
#Write-Host $file_paths.Count, $file_paths
$serial = get_serial_number
$subject = "Files from '$env:COMPUTERNAME' via Bigfix"
$body = "
Hostname: $env:COMPUTERNAME<br/>
Serial Number: $serial
"
Send-MailMessage -To $email -From "endpointmgmt@lbl.gov" -Subject $subject -Body $body -SmtpServer "smtp.lbl.gov" -Attachments $file_objects -BodyAsHtml
#email -fromaddress "endpointmgmt@lbl.gov" -toaddress $email -subject $subject -file_paths $file_paths -body $body