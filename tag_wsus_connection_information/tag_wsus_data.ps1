
Param(
    [string]$datasource_url
)

# This is a shim for where JSON parsing is not available. Previously used system.web.script for JSON deserialization but later found that it's only available in .NET 3.5
$PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent
$json_file = $PSScriptRoot + "\json_shim.ps1"
Import-Module -Name $json_file -Force

function ConvertFrom-JsonShim([object] $item){
    return [Procurios.Public.JSON]::JsonDecode($item)
}

function ConvertTo-JsonShim([object] $item){
    return [Procurios.Public.JSON]::JsonEncode($item)
}

function WriteJsonTo-Registry {
    Param(
        [string]$Path,
        [string]$json_string
    )
    $helper = {
        Param(
            [String]$store,
            [String[]]$path,
            [Object]$obj
        )
        # If array, create a regkey under the given path with the index as the name then recurse for the values
        # If string, create an attribute named value at the given node
        # If hash, create a regkey with that name then recurse for the values
        $string_curpath = "$($store):$($path -join "\")"
        $curpath = Get-ChildItem -Path $string_curpath
        if ($obj -is [Hashtable]){
            ForEach($key in $obj.Keys){
                $arr_new_path = $path + @($key)
                $string_new_path = "$(($arr_new_path) -join "\")"
                New-Item -Path "$($store):$($string_new_path)" | Out-Null
                $helper.Invoke($store, $string_new_path, $obj[$key])
            }
        }
        if (($obj -is [String]) -or ($obj -is [ValueType])){
            Set-ItemProperty -Path $string_curpath -Name "value" -Value $obj | Out-Null
        }
        if (($obj -is [System.Collections.ArrayList]) -or ($obj -is [System.Object[]])) {
            $i = 0
            while ($i -lt $obj.Count){
                $arr_new_path = $path + @($i.ToString())
                $string_new_path = "$(($arr_new_path) -join "\")"
                New-Item -Path "$($store):$($string_new_path)" | Out-Null
                $helper.Invoke($store, $string_new_path, $obj[$i])
                $i += 1
            }
        }
    }
    $store = ($path -split ":")[0]
    $_path = $(($path -split ":")[1] -split '\\')
    $obj = ConvertFrom-JsonShim -item $json_string
    $helper.Invoke($store, $_path, $obj)
}

function log {
    Param(
        [string]$msg
    )
    $log_folder = "$env:SystemDrive\Options\tag_wsus_data"
    $result = mkdir $log_folder -Force
    $timestamp = Get-Date -UFormat "%Y%m%d %H:%M:%S"
    $msg = $timestamp + ": $msg"
    $msg | Add-Content -Path "$log_folder\log.txt"
    Write-Host $msg
}

function write_wsus_data_from
{
    Param(
        [string]$from,
        [string]$susclientid
    )
    $json = wsus_data_from -server $from -susclientid $susclientid
    if ($json -ne $null){
        log -msg "Writing data into the registry"
        $path = "HKLM:\SOFTWARE\LBNL\ICUSW\wsus_data"
        New-Item -Path $path -Force | Out-Null # Destroys if it exists
        WriteJsonTo-Registry -Path $path -json_string $json
    } else {
        log -msg "write_wsus_data_from tried to process null `$data"
    }
}

function Download-String {
    Param(
        [string]$url
    )
    if ([Net.ServicePointManager]::SecurityProtocol.ToString().Contains("Tls12")){
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $WebClient = New-Object System.Net.WebClient
        $retval = $WebClient.DownloadString($url)
    } else {
        $curl = "&`"$PSScriptRoot\curl-7.66.0-win32-mingw.exe`""
        $retval = iex "$curl --cacert `"$PSScriptRoot\icusw-ca-bundle.crt`" -L $url"
    }
    return $retval
}

function wsus_data_from
{
    Param(
        [string]$server,
        [string]$susclientid
    )
    $url = $server + "/api/wsus_data_for?susclientid=$susclientid"
    try {
        $raw_json = Download-String -url $url
        log -msg "Retrieved wsus data"
    } catch {
         log -msg $Error
         return $null
    }
    return $raw_json
}


function get_susclientid
{
  try {
    $retval = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate" -Name "SusClientId").SusClientId
    return $retval
  }
  catch {
    log -msg "Unable to get SusClientId. ERROR:- $Error"
    exit
  }

}

if ($datasource_url -eq "") {
  log -msg "Script called without datasource_url"
}
else {
  $wuserver = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name WUServer).WUServer
  if ($wuserver -match 'wsus.lbl.gov') {
    $susclientid = get_susclientid
    log -msg "Retrieved sus_clientid: $susclientid"
    write_wsus_data_from -from $datasource_url -susclientid $susclientid
  }
  else {
    log -msg "Client not configured to get updates from wsus"
  }

}
