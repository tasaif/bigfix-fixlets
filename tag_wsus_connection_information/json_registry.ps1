﻿# This is a shim for where JSON parsing is not available. Previously used system.web.script for JSON deserialization but later found that it's only available in .NET 3.5
$PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent
$json_file = $PSScriptRoot + "\json_shim.ps1"
Import-Module -Name $json_file -Force
$sample_json1 = '{"key1":"value1","key2":[["string value",{"key3":"value3"}],42,43]}'
$sample_json2 = '[{"foo":"bar"}]'
$sample_json3 = '[{"foo":"bar"},{"fiz":"bin"}]'

function ConvertFrom-JsonShim([object] $item){
    return [Procurios.Public.JSON]::JsonDecode($item)
}

function WriteJsonTo-Registry {
    Param(
        [string]$Path,
        [string]$json_string,
        [boolean]$destructive = $false
    )
    $helper = {
        Param(
            [String]$store,
            [String[]]$path,
            [Object]$obj
        )
        # If array, create a regkey under the given path with the index as the name then recurse for the values
        # If string, create an attribute named value at the given node
        # If hash, create a regkey with that name then recurse for the values
        $string_curpath = "$($store):$($path -join "\")"
        $curpath = Get-ChildItem -Path $string_curpath
        if ($obj -is [Hashtable]){
            ForEach($key in $obj.Keys){
                $arr_new_path = $path + @($key)
                $string_new_path = "$(($arr_new_path) -join "\")"
                New-Item -Path "$($store):$($string_new_path)" | Out-Null
                $helper.Invoke($store, $string_new_path, $obj[$key])
            }
        }
        if (($obj -is [String]) -or ($obj -is [ValueType])){
            Set-ItemProperty -Path $string_curpath -Name "value" -Value $obj | Out-Null
        }
        if (($obj -is [System.Collections.ArrayList]) -or ($obj -is [System.Object[]])) {
            $i = 0
            while ($i -lt $obj.Count){
                $arr_new_path = $path + @($i.ToString())
                $string_new_path = "$(($arr_new_path) -join "\")"
                New-Item -Path "$($store):$($string_new_path)" | Out-Null
                $helper.Invoke($store, $string_new_path, $obj[$i])
                $i += 1
            }
        }
    }
    $store = ($path -split ":")[0]
    $_path = $(($path -split ":")[1] -split '\\')
    $obj = ConvertFrom-JsonShim -item $json_string
    $helper.Invoke($store, $_path, $obj)
}

$path = "HKLM:\SOFTWARE\LBNL\ICUSW\wsus_data"
New-Item -Path $path -Force | Out-Null
WriteJsonTo-Registry -Path $path -json_string $sample_json1
WriteJsonTo-Registry -Path $path -json_string $sample_json2
WriteJsonTo-Registry -Path $path -json_string $sample_json3