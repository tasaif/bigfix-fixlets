#!/bin/bash
url="http://setup.lbl.gov/files/support/LBL%20Unmanaged%20Sophos.dmg"
dmg="sophos_installer.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
curl -L -o $dmg $url
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
/Library/Application\ Support/Sophos/opm-sa/Installer.app/Contents/MacOS/tools/InstallationDeployer --remove
./$dir/Sophos\ Installer.app/Contents/MacOS/tools/InstallationDeployer --install
hdiutil detach $dir
rm $dmg
rm -rf $dir
