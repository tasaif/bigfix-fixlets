#!/usr/bin/env bash

macos_url="https://setup.lbl.gov/files/support/Install%20macOS%20High%20Sierra.dmg"
fname="installer.dmg"
working_directory="/opt/upgrade_to_high_sierra"
hdiutil detach -force "$working_directory/mnt"
rm -rf "$working_directory"
mkdir -p "$working_directory/mnt"
pushd $working_directory
curl -L -o $fname "$macos_url"
hdiutil attach $fname -mountpoint mnt -nobrowse
cp -a mnt/*.app installer.app
hdiutil detach -force mnt 
./installer.app/Contents/Resources/startosinstall --applicationpath "$working_directory/installer.app" --agreetolicense --nointeraction
popd
