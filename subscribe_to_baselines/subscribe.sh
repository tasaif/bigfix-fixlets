#!/bin/bash

baselines_directory="/opt/lbl/baselines"
mkdir -p $baselines_directory
for parm in "$@"
do
  touch $baselines_directory/$parm
done
