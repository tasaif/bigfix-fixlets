﻿$PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent
$json_file = $PSScriptRoot + "\json.ps1"
Import-Module -Name $json_file -Force

function Get-DriverInformation {
    Get-WmiObject Win32_PnPSignedDriver| select devicename, driverversion
}

function get_serial_number()
{
    return (Get-WmiObject -Class Win32_BIOS).SerialNumber
}

function post {
    Param(
        [string]$url,
        [string]$data
    )
    $data = "data=" + $data + "&sheet_name=" + $env:COMPUTERNAME
    $Body = [byte[]][char[]]$data;
    $request = [System.Net.WebRequest]::Create("$url")
    $request.Method = 'POST';
    $request.ContentType='application/x-www-form-urlencoded'
    $Stream = $Request.GetRequestStream();
    $Stream.Write($Body, 0, $Body.Length);
    $Stream.Close();
    $response = $Request.GetResponse();
    $ResponseStream = $Response.GetResponseStream()
    $ReadStream = New-Object System.IO.StreamReader $ResponseStream
    $response_data = $ReadStream.ReadToEnd()
    return $response_data
}

function generate_and_log_data {
    $driver_information = Get-DriverInformation
    $json_string = ConvertTo-STJson -InputObject $driver_information
    $retval = post -url "https://script.google.com/a/lbl.gov/macros/s/AKfycbxIIVMMmR_kG5T1IwAxg1DoOaXt5rHVt9Zu41TtBHY3ySPz4Kc/exec" -data $json_string
    return $retval
}

generate_and_log_data