﻿$PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent
$SaveExecutionPolicy = Get-ExecutionPolicy 
#  
Set-ExecutionPolicy RemoteSigned -Scope Currentuser 
 
Import-Module "$PSScriptRoot\SpeculationControl"
$settings = Get-SpeculationControlSettings
Set-ExecutionPolicy $SaveExecutionPolicy -Scope Currentuser

$reg_path = "HKLM:\SOFTWARE\LBNL\Incidents\INC0168837"
New-Item -Path $reg_path -Force | Out-Null
ForEach($property in $settings.psobject.properties){
    New-ItemProperty -Path $reg_path -Value $property.Value -Name $property.Name -Force | Out-Null    
}
