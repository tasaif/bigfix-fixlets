require 'slim'
require 'pry'

scope = {
  reg_keys: {
    INC0168837: [
      'BTIHardwarePresent',
      'BTIWindowsSupportPresent',
      'BTIWindowsSupportEnabled',
      'BTIDisabledBySystemPolicy',
      'BTIDisabledByNoHardwareSupport',
      'KVAShadowRequired',
      'KVAShadowWindowsSupportPresent',
      'KVAShadowWindowsSupportEnabled',
      'KVAShadowPcidEnabled'
    ]
  }
}
puts Slim::Template.new("analysis_template.slim").render(scope)
