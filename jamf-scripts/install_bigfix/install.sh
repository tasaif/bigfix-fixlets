#!/bin/bash
url="https://setup.lbl.gov/files/support/LBL%20BigFix%209.5.5.196-r1.dmg"
dmg="bigfix.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
# Install new
installer -pkg ./$dir/*.pkg -target /
# Cleanup
hdiutil detach $dir
rm $dmg
rm -rf $dir
jamf recon
