#!/bin/bash
url="https://download.mozilla.org/?product=firefox-latest-ssl&os=osx&lang=en-US"
curl -L -o firefox_installer.dmg $url
mkdir firefox_installer
hdiutil attach firefox_installer.dmg -mountpoint firefox_installer -nobrowse
rm -rf /Applications/Firefox.app
cp -r firefox_installer/Firefox.app /Applications/Firefox.app
hdiutil detach firefox_installer
rm firefox_installer.dmg
rm -rf firefox_installer
