﻿$PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent
$json_file = $PSScriptRoot + "\json_shim.ps1"
Import-Module -Name $json_file -Force
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

function log {    Param(        [string]$msg    )    $log_folder = "$env:SystemDrive\Options\tag_property_data"    $result = mkdir $log_folder -Force	$timestamp = Get-Date -UFormat "%Y%m%d %H:%M:%S"	$msg = $timestamp + ": $msg"    $msg | Add-Content -Path "$log_folder\log.txt"    Write-Host $msg}

function ConvertFrom-JsonShim([object] $item){     return [Procurios.Public.JSON]::JsonDecode($item)}
function GetOrCreate-Item {    Param(        [string]$Path    )    $exists = Test-Path -Path $Path
    if ($exists){
        return get-item -Path $path
    }
    $retval = new-item -Path $path
    return $retval
}
function psobject_to_hash($object){
    $retval = @{}
    $object.psobject.properties | Foreach { $retval[$_.Name] = $_.Value }
    return $retval
}
function dump-json-at {
    Param(
        [string]$Path,
        [PSCustomObject]$Object,
        [int]$count
    )
    GetOrCreate-Item -Path $path | Out-Null
    if ($Object -eq $null){
        return 0
    }
    if ($Object.getType().ToString() -like "*PSCustomObject*"){
        $Object = psobject_to_hash($Object)
    }
    foreach ($key in $Object.keys){
        $value = $Object[$key]
        if ($value -ne $null){
            if (
                $value.gettype().toString() -like "*Dictionary*" -or
                $value.gettype().name -like "*Hashtable*" -or
                $value.gettype().toString() -like "*PSCustomObject*"
            ){
                $count += (dump-json-at -Path "$path\$key" -Object $value)
            } else {
                New-ItemProperty -Path $path -Value $value -Name $key -Force | Out-Null
                $count += 1
            }
        }
    }
    return $count
}

function get-wirelessnetworks {
    $results = netsh wlan show networks mode=bssid
    $prefixes_to_ignore = @(
        "Interface name : ",
        "There are"
    )
    $network = @{}
    $networks = @{}
    $ssid = ""
    $bssid = ""
    ForEach($line in $results){
        $line = $line.Trim()
        if ($line.Length -eq 0){
            continue;
        }
        if (($prefixes_to_ignore | ? { $line.StartsWith($_) }).Count -gt 0){
            continue;
        }
        $record = ($line -split ':',2) | % { $_.Trim() }
        if ($line.StartsWith("SSID")){
            $ssid = $record[1]
            $bssid = ""
            if ($ssid.Length -ne 0){
                $networks[$ssid] = @{}
                $networks[$ssid]["bssid"] = @{}
            }
            continue;
        }
        if ($ssid.Length -eq 0){
            continue;
        }
        if ($line.StartsWith("BSSID")){
            $bssid = $record[1]
            #while ($true){ $input = Read-Host -Prompt "repl"; if ($input -eq "exit"){ return 0 }; Invoke-Expression $input }
            $networks[$ssid]["bssid"][$bssid] = @{}
            continue;
        }
        if ($bssid.Length -eq 0){
            $networks[$ssid][$record[0]] = $record[1]
        } else {
            $networks[$ssid]["bssid"][$bssid][$record[0]] = $record[1]
        }
    }
    return $networks
}

$networks = get-wirelessnetworks
GetOrCreate-Item -Path "HKLM:\SOFTWARE\LBNL\ICUSW"
$path = "HKLM:\SOFTWARE\LBNL\ICUSW\wireless_networks"
$records_written = dump-json-at -Path $path -Object $networks -count 0
log -msg "Wrote $records_written records"
Set-ItemProperty -Path "HKLM:\SOFTWARE\LBNL\ICUSW\wireless_networks" -Name "json" -Value (((($networks | ConvertTo-JsonNewtonsoft) -split "`n") | % { $_.Trim() }) -join "")