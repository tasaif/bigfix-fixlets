﻿$shortcuts = @(
    "Creo Modelcheck 3.0 M130.lnk",
    "Creo Parametric 3.0 M130.lnk",
    "Creo Simulate 3.0 M130.lnk"
)
$files = @(
    "Week2.zip",
    "T4501_Intro_Week_1.zip",
    "Advanced_Week_3.zip",
    "Creo_3_M130.zip"
)
$url_prefix = "http://setup.lbl.gov/files/support/TrainingRoom"
$path = $env:SystemDrive + "\Options\Creo"
$unzip_prefix = $env:SystemDrive + "\CreoTraining"

# Ensure the directories we're working in exist are all clearn
rm -Path $path -Recurse -Force -ErrorAction Ignore
rm -Path $unzip_prefix -Recurse -Force -ErrorAction Ignore
mkdir -Path $path -Force
mkdir -Path $unzip_prefix -Force

# Download each file in the Options folder and extract into c:\CreoTraining
foreach($file in $files){
    $url = "$url_prefix/$file"
    $target = "$path\$file"
    $unzip_directory = "$unzip_prefix\" + $file.split('.')[0]
    wget $url -UseBasicParsing -OutFile $target
    Expand-Archive -Path $target -DestinationPath $unzip_directory 
}

# Install Creo and remove the Desktop shortcuts
msiexec.exe /i "$unzip_prefix\Creo_3_M130\Creo_3_M120.msi" /qn
foreach($shortcut in $shortcuts){
    $path = $env:SystemDrive + "\Users\Public\Desktop\$shortcut"
    rm $path
}