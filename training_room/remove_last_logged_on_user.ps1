﻿$path = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI\"
$properties = @(
    'LastLoggedOnUserSID',
    'LastLoggedOnDisplayName',
    'LastLoggedOnUser',
    'LastLoggedOnProvider',
    'LastLoggedOnSAMUser'
)
foreach($property in $properties){
    Remove-ItemProperty -Path $path -Name $property -ErrorAction SilentlyContinue
}