import json
import os
import sys
import subprocess
import logging
try:
  import urllib.request as compat_urllib_request
except ImportError: # Python 2
  import urllib2 as compat_urllib_request

class IcuswComputer:

  def __init__(self):
    self.base_dir = '/opt/lbl/icusw'
    try:
      p1 = subprocess.Popen(['dmidecode', '-t', 'system'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      p2 = subprocess.Popen(['grep', 'Serial'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=p1.stdout)
      p1.stdout.close()
      p3 = subprocess.Popen(['awk', '{print $3}'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=p2.stdout)
      p2.stdout.close()
      self.serial_number,err = p3.communicate()
      if err:
        raise Exception("Failed to find serial number: %s"%(err))
    except (OSError, ValueError) as e:
      raise Exception("%s: %s"%(type(e),str(e)))
    self.create_dir(self.base_dir)

  def create_dir(self,path):
    logging.info("Creating Dir %s"%(path))
    if not os.path.exists(path):
      try:
        os.makedirs(path)
      except OSError:
        raise Exception("Couldn't create directory %s"%(path))
    else:
      logging.warning("Directory %s already exists!"%(path))

  def get_property_data(self,server):
    try:
      response = compat_urllib_request.urlopen("%s/api/property_data_for?serial_number=%s"%(server,self.serial_number.strip()))
      return json.load(response)
    except (compat_urllib_request.HTTPError, compat_urllib_request.URLError) as e:
      raise Exception("%s: %s"%(type(e),str(e.reason)))

  def write_property_data(self,server):
    if not os.path.exists(self.base_dir):
      raise Exception("Warning: Not writing keys, because couldn't verify that working directory: %s exists"%(base_dir))
    data = self.get_property_data(server)
    logging.info("Writing property data...")
    self.write_data_helper(self.base_dir,data)

  def write_data_helper(self,dir,data):
    for (key,value) in data.items():
      logging.info("Property %s : %s"%(key,value))
      new_path = dir+'/'+key
      if (type(value).__name__ == 'dict'):
        self.create_dir(new_path)
        self.write_data_helper(new_path,value)
      else:
        file = open(new_path, 'w+')
        file.write(str(value)+'\n')
        file.close

try:
  logging.basicConfig(filename="%s.log"%(__file__[:-3]),level=logging.DEBUG,format="%(asctime)s - %(levelname)s - %(message)s")
  if len(sys.argv) != 2: raise Exception("Usage: python %s <server>"%(__file__))
  server = sys.argv[1]
  localhost = IcuswComputer()
  localhost.write_property_data(server)
except Exception as e:
  logging.error("Error: %s"%(str(e)))
